# KsMf
**KsMf** is a modular microframework for create minimalistic web application or REST API based on [Express Js](https://expressjs.com/es/) and [KsDp](https://github.com/ameksike/ksdp/wiki), for more information see our [wiki](https://github.com/ameksike/ksmf/wiki).

[![Rate on Openbase](https://badges.openbase.com/js/rating/ksmf.svg)](https://openbase.com/js/ksmf?utm_source=embedded&utm_medium=badge&utm_campaign=rate-badge)

### Get started
1. [Install](https://github.com/ameksike/ksmf/wiki/Install)
2. [Hello World](https://github.com/ameksike/ksmf/wiki/Hello-World)

### Common topics
1. [Project Skeleton](https://github.com/ameksike/ksmf/wiki/Project-Skeleton)
2. [Modules](https://github.com/ameksike/ksmf/wiki/Modules)
3. [Controllers](https://github.com/ameksike/ksmf/wiki/Controllers)
4. [Services](https://github.com/ameksike/ksmf/wiki/Services)
5. [Data Base](https://github.com/ameksike/ksmf/wiki/DAO)

### Advanced topics
1. [Test](https://github.com/ameksike/ksmf/wiki/Test)
2. [Web App](https://github.com/ameksike/ksmf/wiki/App) 
3. [CLI App](https://github.com/ameksike/ksmf/wiki/App-CLI) 
4. [Settings](https://github.com/ameksike/ksmf/wiki/Setting)
5. [Routes](https://github.com/ameksike/ksmf/wiki/Routes)
6. [Events](https://github.com/ameksike/ksmf/wiki/Events)
7. [Cron / Schedule](https://github.com/ameksike/ksmf/wiki/Cron)
8. [Dashboard](https://github.com/ameksike/ksmf/wiki/Dashboard)

This is a project example that is based on this framework: [API.Heroku](https://github.com/ameksike/api.heroku) 
